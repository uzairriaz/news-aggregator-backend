<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BaseModel extends Model
{
    public function scopeNameNotEmpty(Builder $builder) {
        return $builder->whereNotNull('name')->whereNot('name', '');
    }

    public function scopePreferred(Builder $builder) {
        /** @var User $user */
        $user = Auth::user();

        return $builder->when($user->preference, function(Builder $builder) use ($user) {
            $builder->whereIn('id', $user->preference->{$this->getTable()});
        });
    }
}
