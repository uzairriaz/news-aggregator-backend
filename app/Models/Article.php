<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Article extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'content', 'url', 'image_url', 'author_id', 'source_id', 'category_id', 'published_at'];

    public function author() {
        return $this->belongsTo(Author::class);
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function source() {
        return $this->belongsTo(Source::class);
    }

    public function scopePreferred(Builder $builder) {
        /** @var User $user */
        $user = Auth::user();

        return $builder->when($user->preference, function (Builder $builder) use ($user) {
            $authors = $user->preference->authors;
            $categories = $user->preference->categories;
            $sources = $user->preference->sources;

            $builder->when(!empty($authors), function(Builder $builder) use ($authors) {
                $builder->whereIn('author_id', $authors);
            })->when(!empty($categories), function(Builder $builder) use ($categories) {
                $builder->whereIn('category_id', $categories);
            })->when(!empty($sources), function(Builder $builder) use ($sources) {
                $builder->whereIn('source_id', $sources);
            });
        });
    }
}
