<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Author extends BaseModel
{
    use HasFactory;

    protected $fillable = ['name'];
    public $timestamps = false;
}
