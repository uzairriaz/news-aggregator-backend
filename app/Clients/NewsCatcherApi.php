<?php

namespace App\Clients;

use Carbon\Carbon;
use GuzzleHttp\Client;

class NewsCatcherApi implements NewsApi
{
    private $apiKey;
    private $pageSize;

    public function __construct($apiKey, $pageSize)
    {
        $this->apiKey = $apiKey;
        $this->pageSize = $pageSize;
    }

    public function fetchNews($page = 1)
    {
        $client = new Client(['base_uri' => 'https://api.newscatcherapi.com']);

        $response = $client->get('/v2/search', [
            'headers' => [
                'x-api-key' => $this->apiKey,
            ],
            'query' => [
                'q' => 'technology',
                'sort_by' => 'relevancy',
                'page' => $page,
                'page_size' => $this->pageSize,
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            throw new \RuntimeException('Failed to fetch news from Newscatcher API: ' . $response->getBody());
        }

        $data = json_decode($response->getBody(), true);
        $articles = $data['articles'];


        $news = [];
        foreach ($articles as $article) {
            $news[] = [
                'title' => $article['title'],
                'description' => $article['excerpt'],
                'content' => $article['summary'],
                'url' => $article['link'],
                'image_url' => $article['media'],
                'published_at' => $article['published_date'],
                'author' => $article['author'],
                'source' => $article['clean_url'],
                'category' => $article['topic'],
            ];
        }

        return [
            'total' => $data['total_hits'],
            'news' => $news
        ];
    }
}
