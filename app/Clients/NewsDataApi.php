<?php

namespace App\Clients;

use Carbon\Carbon;
use GuzzleHttp\Client;

class NewsDataApi implements NewsApi
{
    private $apiKey;

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function fetchNews()
    {
        $client = new Client(['base_uri' => 'https://newsdata.io/api/1/']);

        $response = $client->request('GET', 'news', [
            'query' => [
                'q' => 'tech',
                'apikey' => $this->apiKey,
            ],
        ]);

        if ($response->getStatusCode() != 200) {
            throw new \RuntimeException('Failed to fetch news from NewsData API: ' . $response->getBody());
        }

        $response = json_decode($response->getBody(), true);
        $articles = $response['results'];

        $news = [];
        foreach ($articles as $article) {
            $news[] = [
                'title' => $article['title'],
                'description' => $article['description'],
                'content' => $article['content'],
                'url' => $article['link'],
                'image_url' => $article['image_url'],
                'published_at' => Carbon::parse($article['pubDate'])->toDateTimeString(),
                'source' => $article['source_id'],
                'category' => $article['category'] ? $article['category'][0] : null,
                'author' => null
            ];
        }

        return [
            'total' => count($news),
            'news' => $news
        ];
    }
}
