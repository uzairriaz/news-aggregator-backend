<?php

namespace App\Clients;

interface NewsApi
{
    public function fetchNews();
}
