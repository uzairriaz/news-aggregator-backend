<?php

namespace App\Clients;

use Carbon\Carbon;
use GuzzleHttp\Client;

class GoogleNewsApi implements NewsApi
{
    private $apiKey;
    private $pageSize;

    public function __construct($apiKey, $pageSize)
    {
        $this->apiKey = $apiKey;
        $this->pageSize = $pageSize;
    }

    public function fetchNews(int $page = 1)
    {
        $client = new Client([
            'base_uri' => 'https://newsapi.org/v2/',
            'timeout'  => 2.0,
        ]);

        $response = $client->request('GET', 'everything', [
            'query' => [
                'apiKey' => $this->apiKey,
                'page' => $page,
                'pageSize' => $this->pageSize,
                'q' => '"a OR b OR c or D or E"'
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            throw new \RuntimeException('Failed to fetch news from GoogleNews API: ' . $response->getBody());
        }

        $data = json_decode($response->getBody(), true);
        $articles = $data['articles'];

        $news = [];
        foreach ($articles as $article) {
            $news[] = [
                'title' => $article['title'],
                'description' => $article['description'],
                'content' => $article['content'],
                'author' => $article['author'],
                'url' => $article['url'],
                'image_url' => $article['urlToImage'],
                'source' => $article['source']['name'],
                'published_at' => Carbon::parse($article['publishedAt'])->toDateTimeString(),
                'category' => null,
            ];
        }

        return [
            'total' => $data['totalResults'],
            'news' => $news
        ];
    }
}
