<?php

namespace App\Console\Commands;

use App\NewsAggregator;
use Illuminate\Console\Command;

class SyncNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:news';

    private $aggregator;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download news from data sources to local database';

    public function __construct(NewsAggregator $aggregator)
    {
        parent::__construct();
        $this->aggregator = $aggregator;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->aggregator->syncNews();
    }
}
