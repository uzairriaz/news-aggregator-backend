<?php

namespace App\Providers;

use App\Clients\GoogleNewsApi;
use App\Clients\NewsCatcherApi;
use App\Clients\NewsDataApi;
use App\NewsAggregator;
use App\Repositories\ArticleRepository;
use App\Repositories\AuthorRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\SourceRepository;
use Illuminate\Support\ServiceProvider;

class NewsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(NewsAggregator::class, function ($app) {
            return new NewsAggregator(
                [
                    new GoogleNewsApi(config('news.google_news_api_key'), config('news.page_size')),
                    new NewsCatcherApi(config('news.news_catcher_api_key'), config('news.page_size')),
                    new NewsDataApi(config('news.news_data_api_key')),
                ],
                new ArticleRepository(),
                new AuthorRepository(),
                new CategoryRepository(),
                new SourceRepository()
            );
        });
    }
}
