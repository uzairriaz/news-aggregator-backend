<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Category;
use App\Models\Source;
use App\Models\User;
use App\Repositories\PreferenceRepository;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PreferenceController extends Controller
{
    protected $preferenceRepository;

    public function __construct(PreferenceRepository $preferenceRepository)
    {
        $this->preferenceRepository = $preferenceRepository;
    }

    public function get(Request $request) {
        /** @var User $user */
        $user = $request->user();
        $authors = Author::find($user->preference->authors);
        $categories = Category::find($user->preference->categories);
        $sources = Source::find($user->preference->sources);

        return response()->json([
            'authors' => $authors,
            'categories' => $categories,
            'sources' => $sources,
        ], Response::HTTP_OK);
    }

    public function store(Request $request) {
        /** @var User $user */
        $user = $request->user();
        $data = array_merge(['user_id' => $user->id], $request->all());

        $this->preferenceRepository->save($data);

        return response()->json(['message' => 'Preferences saved successfully!'], Response::HTTP_OK);
    }
}
