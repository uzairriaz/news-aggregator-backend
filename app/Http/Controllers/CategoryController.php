<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        return Category::nameNotEmpty()
            ->when($request->get('preferred'), function(Builder $builder) {
                $builder->preferred();
            })
            ->get();
    }
}
