<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function index(Request $request)
    {
        return Author::nameNotEmpty()
            ->when($request->get('preferred'), function(Builder $builder) {
                $builder->preferred();
            })
            ->get();
    }
}
