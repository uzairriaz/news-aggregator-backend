<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(Request $request)
    {
        $q = $request->get('q');
        $categoryId = $request->get('category_id');
        $sourceId = $request->get('source_id');
        $date = $request->get('date');

        return Article::with('author')->when($q, function (Builder $builder) use ($q) {
            $builder->where(function (Builder $builder) use ($q) {
                $builder->where('title', 'LIKE', '%' . $q . ' %')
                    ->orWhere('description', 'LIKE', '%' . $q . ' %')
                    ->orWhere('content', 'LIKE', '%' . $q . ' %');
            });
        })->when($categoryId, function (Builder $builder) use ($categoryId) {
            $builder->where('category_id', $categoryId);
        })->when($sourceId, function (Builder $builder) use ($sourceId) {
            $builder->where('source_id', $sourceId);
        })->when($date, function (Builder $builder) use ($date) {
            $builder->whereDate('published_at', $date);
        })->preferred()->paginate(20);
    }

    public function get(Article $article) {
        return $article;
    }
}
