<?php

namespace App\Http\Controllers;

use App\Models\Source;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class SourceController extends Controller
{
    public function index(Request $request)
    {
        return Source::nameNotEmpty()
            ->when($request->get('preferred'), function(Builder $builder) {
                $builder->preferred();
            })
            ->get();
    }
}
