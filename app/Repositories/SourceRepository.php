<?php

namespace App\Repositories;

use App\Models\Source;

class SourceRepository implements Repository
{
    public function save(array $data)
    {
        return Source::firstOrCreate($data);
    }
}
