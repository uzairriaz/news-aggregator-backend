<?php

namespace App\Repositories;

interface Repository
{
    public function save(array $data);
}
