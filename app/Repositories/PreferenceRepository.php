<?php

namespace App\Repositories;

use App\Models\Preference;

class PreferenceRepository implements Repository
{
    public function save(array $data)
    {
        return Preference::updateOrCreate(
            ['user_id' => $data['user_id']],
            $data
        );
    }
}
