<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository implements Repository
{
    public function save(array $data)
    {
        return Category::firstOrCreate($data);
    }
}
