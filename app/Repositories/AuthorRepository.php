<?php

namespace App\Repositories;

use App\Models\Author;

class AuthorRepository implements Repository
{
    public function save(array $data)
    {
        return Author::firstOrCreate($data);
    }
}
