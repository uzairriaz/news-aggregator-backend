<?php

namespace App\Repositories;

use App\Models\Article;

class ArticleRepository implements Repository
{
    public function save(array $data)
    {
        return Article::updateOrCreate(
            [
                'title' => $data['title'],
                'description' => $data['description'],
                'url' => $data['url'],
                'published_at' => $data['published_at'],
            ],
            $data
        );
    }
}
