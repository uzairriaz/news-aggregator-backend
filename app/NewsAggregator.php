<?php

namespace App;

use App\Clients\NewsApi;
use App\Repositories\ArticleRepository;
use App\Repositories\AuthorRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\SourceRepository;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class NewsAggregator {
    private $apis;

    private $articleRepository;
    private $authorRepository;
    private $categoryRepository;
    private $sourceRepository;
    private $logger;

    public function __construct(array $apis, ArticleRepository $articleRepository, AuthorRepository $authorRepository, CategoryRepository $categoryRepository, SourceRepository $sourceRepository) {
        $this->apis = $apis;
        $this->articleRepository = $articleRepository;
        $this->authorRepository = $authorRepository;
        $this->categoryRepository = $categoryRepository;
        $this->sourceRepository = $sourceRepository;
        $this->logger = Log::channel('news');
    }

    public function syncNews() {
        /** @var NewsApi $api */
        foreach ($this->apis as $api) {
            $this->logger->info('Fetching news from '. $api::class);
            try {
                $page = 1;
                $pageSize = config('news.page_size');
                do {
                    $this->logger->info('Fetching page ' . $page);
                    $data = $api->fetchNews($page);
                    $this->logger->info('Page ' . $page . ' fetching successful');

                    $this->logger->info('Saving page ' . $page);
                    DB::transaction(function () use ($data) {
                        foreach ($data['news'] as $newsItem) {
                            $author = $this->authorRepository->save(['name' => $newsItem['author']]);
                            $category = $this->categoryRepository->save(['name' => $newsItem['category']]);
                            $source = $this->sourceRepository->save(['name' => $newsItem['source']]);

                            $newsItem['author_id'] = $author->id;
                            $newsItem['category_id'] = $category->id;
                            $newsItem['source_id'] = $source->id;

                            $this->articleRepository->save($newsItem);
                        }
                    });
                    $this->logger->info('Page ' . $page . ' saved successfully');

                    $page++;
                    $total = $data['total'];
                } while ($page <= ceil($total / $pageSize));
            } catch (ClientException $ex) {
                $this->logger->error($ex->getMessage());
            }
        }
    }
}
