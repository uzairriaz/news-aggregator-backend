<?php

return [
    'google_news_api_key' => env('GOOGLE_NEWS_API_KEY'),
    'news_catcher_api_key' => env('NEWS_CATCHER_API_KEY'),
    'news_data_api_key' => env('NEWS_DATA_API_KEY'),
    'page_size' => 20,
];
