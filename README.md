Prerequisites
-----

Have Docker installed and running on your machine.

See the [Docker website](http://www.docker.io/gettingstarted/#h_installation) for installation instructions.

Build
-----

Steps to build the project with Docker:

1. Clone this repo

        git clone git@gitlab.com:uzairriaz/news-aggregator-backend.git

2. **Important:** Change to the repo directory and copy `.env.example` and rename it to `.env`

        cd news-aggregator-backend
        cp .env.example .env

3. **Important:** Fill out these `.env` variables with the API keys from respective sites

        # https://newsapi.org/register
        GOOGLE_NEWS_API_KEY=
        # https://newsdata.io/register
        NEWS_DATA_API_KEY=
        # https://app.newscatcherapi.com/auth/register
        NEWS_CATCHER_API_KEY=

4. Build and run the project (This will take a few minutes.)

        docker compose up

5. Once everything has started up, you should be able to access the api via [http://localhost:8000/](http://localhost:8000/) on your host machine.

        http://localhost:8000/

Technical Details
-----
This project uses docker to spin up three service containers:

- `mysql`: MySQL server that acts as the local database for the project. Project is DB agnostic, however.
- `api`: Laravel application container that handles authentication tokens and exposes other APIs to the frontend module.
- `scheduler`: Same as `api` but this one focuses solely on running the scheduled tasks. It's entrypoint is set in a way that it executes the artisan command `schedule:run` every 60 seconds. `sync:news` is the artisan command that is currently scheduled to run every thirty minutes.
